package com.scheduler.idadiani.smsschedule.schedulehistory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.nhaarman.listviewanimations.util.Insertable;
import com.scheduler.idadiani.smsschedule.scheduledetails.ActivityScheduleDetails;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by i.dadiani on 5/21/2015.
 */
public class ScheduleHistorySwipeAdapter extends BaseSwipeAdapter implements Insertable {

    public static final String SCHEDULE_ID = "schedule_id";
    private Context mContext;
    private List<ScheduleModel> mScheduleModels;

    public ScheduleHistorySwipeAdapter(Context mContext) {
        this.mContext = mContext;
        this.mScheduleModels = new ArrayList<>();
    }

    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int position, ViewGroup viewGroup) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_schedule_swipe, null);
        SwipeLayout swipeLayout = (SwipeLayout) convertView.findViewById(getSwipeLayoutResourceId(position));

        swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.edit));

        convertView.findViewById(R.id.edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScheduleModel scheduleModel = mScheduleModels.get(position);
                Intent scheduleEditIntent = new Intent(mContext, ActivityScheduleDetails.class);
                scheduleEditIntent.putExtra(SCHEDULE_ID, scheduleModel.getId());
                closeAllItems();
                mContext.startActivity(scheduleEditIntent);
            }
        });

        convertView.findViewById(R.id.item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeAllItems();
            }
        });
        return convertView;
    }

    @Override
    public void fillValues(int position, View convertView) {
        if (convertView == null) {
            LayoutInflater mInflater =
                    (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_schedule_swipe, null);
        }
        final ScheduleModel scheduleModel = mScheduleModels.get(position);

        if (scheduleModel != null) {
            TextView textViewDate = (TextView) convertView.findViewById(R.id.schedule_date);
            TextView textViewMessage = (TextView) convertView.findViewById(R.id.schedule_message);
            TextView textViewTitle = (TextView) convertView.findViewById(R.id.title);

            ImageView imageView = (ImageView) convertView.findViewById(R.id.icon);

            final DateFormat DateFormat = android.text.format.DateFormat
                    .getMediumDateFormat(mContext.getApplicationContext());
            String pattern = ((SimpleDateFormat) DateFormat).toPattern();
            SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
            Calendar calendar = scheduleModel.getSendingDate();

            String date = format.format(calendar.getTime());
            textViewDate.setText(date);
            textViewMessage.setText(scheduleModel.getMessage());
            textViewTitle.setText(scheduleModel.getTitle());

            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color2 = generator.getColor(Integer.toString(calendar.get(Calendar.MONTH)));

            TextDrawable drawable2 = TextDrawable.builder()
                    .buildRound(Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)), color2);

            imageView.setImageDrawable(drawable2);
        }
    }

    @Override
    public int getCount() {
        return mScheduleModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void add(int i, @NonNull Object o) {

    }

    public void setItems(List<ScheduleModel> items) {
        mScheduleModels = items;
        notifyDataSetChanged();
    }
}
