package com.scheduler.idadiani.smsschedule.schedulelist;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

import java.util.List;

/**
 * Created by idadiani on 15/09/16.
 */
public interface ScheduleListView {
    void populateList(List<ScheduleModel> list);
    void startNewScheduleCreation();
    void showHistory();
    void showDetailed(ScheduleModel scheduleModel);
}
