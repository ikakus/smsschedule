package com.scheduler.idadiani.smsschedule.classes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.scheduler.idadiani.smsschedule.services.SmsSenderService;

/**
 * Created by 404 on 4/11/2015.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Long id = intent.getLongExtra("schedule_id", -1);

        Intent serviceIntent = new Intent(context, SmsSenderService.class);
        serviceIntent.putExtra("schedule_id",id);
        context.startService(serviceIntent);

    }
}
