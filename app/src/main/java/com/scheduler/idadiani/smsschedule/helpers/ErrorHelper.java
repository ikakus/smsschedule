package com.scheduler.idadiani.smsschedule.helpers;

import android.content.Context;

import com.scheduler.idadiani.smsschedule.R;

/**
 * Created by idadiani on 15/09/16.
 */
public class ErrorHelper {
    public static String getMessage(Context context,int code){
        String message = "Unknown error";

        switch (code) {
            case 0:
                return context.getResources().getString(R.string.schedule_date_error);
            case 1:
                return context.getResources().getString(R.string.schedule_contacts_error);
            case 2:
                return context.getResources().getString(R.string.schedule_message_error);
        }

        return message;
    }
}
