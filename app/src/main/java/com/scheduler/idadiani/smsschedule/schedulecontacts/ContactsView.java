package com.scheduler.idadiani.smsschedule.schedulecontacts;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;

import java.util.List;

/**
 * Created by idadiani on 10/10/16.
 */

public interface ContactsView {
    void populateList(List<ContactModel> list);
    void setItemChecked(int id, boolean b);
    void returnSelectedContacts(List<ContactModel> selectedItems);
}
