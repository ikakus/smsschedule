package com.scheduler.idadiani.smsschedule.mvp.domain.repository;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

import java.util.List;

/**
 * Created by idadiani on 13/09/16.
 */
public interface RepositoryInterface {
    void addSchedule(ScheduleModel scheduleModel);

    void deleteSchedule(ScheduleModel scheduleModel);

    void updateSchedule(ScheduleModel scheduleModel);

    ScheduleModel getScheduleById(long id);

    List<ScheduleModel> getAllSchedules();

    List<ScheduleModel> getArchivedSchedules();
}

