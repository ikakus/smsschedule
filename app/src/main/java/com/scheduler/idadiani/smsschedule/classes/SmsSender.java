package com.scheduler.idadiani.smsschedule.classes;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;

/**
 * Created by 404 on 4/10/2015.
 */
public class SmsSender {

    private Context mContext;
    PendingIntent mSentPI;
    PendingIntent mDeliveredPI;
    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    public SmsSender(Context context) {
        mContext = context;
    }

    public void sendSMS(String phoneNumber, String message) {
        mSentPI = PendingIntent.getBroadcast(mContext, 0, new Intent(SENT), 0);
        mDeliveredPI = PendingIntent.getBroadcast(mContext, 0, new Intent(DELIVERED), 0);

        SmsManager sms = SmsManager.getDefault();
        try {
            sms.sendTextMessage(phoneNumber, null, message, mSentPI, mDeliveredPI);
        } catch (Exception ex) {
            // TODO: exception logging
        }
    }

}
