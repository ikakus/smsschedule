package com.scheduler.idadiani.smsschedule.enums;

/**
 * Created by idadiani on 15/09/16.
 */
public final class Error {
   public static int SET_DATE = 0;
   public static int SET_CONTACTS = 1;
   public static int SET_MESSAGE = 2;
}
