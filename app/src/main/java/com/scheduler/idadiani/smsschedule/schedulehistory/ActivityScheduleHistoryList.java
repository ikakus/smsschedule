package com.scheduler.idadiani.smsschedule.schedulehistory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AbsListView;

import com.daimajia.swipe.util.Attributes;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityScheduleHistoryList extends AppCompatActivity implements ScheduleHistoryView {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(android.R.id.list)
    DynamicListView mSchedulesListView;
    private ScheduleHistorySwipeAdapter mSchedulesAdapter;
    private ScheduleHistoryPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_history_list);
        ButterKnife.bind(this);

        mSchedulesAdapter = new ScheduleHistorySwipeAdapter(this);
        mSchedulesAdapter.setMode(Attributes.Mode.Single);

        ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(mSchedulesAdapter);
        mSchedulesListView.setAdapter(animationAdapter);

        mPresenter = new ScheduleHistoryPresenter();
        mPresenter.setView(this);

        setToolbar();
        setListeners();
    }

    private void setListeners() {
        mSchedulesListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mSchedulesAdapter.closeAllItems();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.history_page_title);
    }

    public void fillList(List<ScheduleModel> scheduleModels) {
        mSchedulesAdapter.setItems(scheduleModels);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void populateList(List<ScheduleModel> schedules) {
        fillList(schedules);
    }
}