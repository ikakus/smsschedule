package com.scheduler.idadiani.smsschedule.mvp.domain.model;

import com.scheduler.idadiani.smsschedule.enums.ContactStatus;

import java.io.Serializable;

/**
 * Created by i.dadiani on 4/8/2015.
 */
public class ContactModel implements Serializable {
    private ContactStatus mContactStatus;
    private String mPhoneNumber;
    private String mName;
    private long mId;

    public ContactModel(long id, String name, String phoneNumber) {
        setName(name);
        setPhoneNumber(phoneNumber);
        setId(id);
        setContactStatus(ContactStatus.PENDING);
    }

    public ContactModel(long id, String name, String phoneNumber, ContactStatus contactStatus) {
        setName(name);
        setPhoneNumber(phoneNumber);
        setId(id);
        setContactStatus(contactStatus);
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.mPhoneNumber = phoneNumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public ContactStatus getContactStatus() {
        return mContactStatus;
    }

    public void setContactStatus(ContactStatus contactStatus) {
        this.mContactStatus = contactStatus;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this.getName().equals(((ContactModel) o).getName())) {
            if (this.getPhoneNumber().equals(((ContactModel) o).getPhoneNumber())) {
                return true;
            }
        }
        return false;
    }
}
