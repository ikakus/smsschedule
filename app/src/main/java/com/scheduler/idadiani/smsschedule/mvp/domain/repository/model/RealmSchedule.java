package com.scheduler.idadiani.smsschedule.mvp.domain.repository.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by i.dadiani on 4/8/2015.
 */
public class RealmSchedule extends RealmObject {
    @PrimaryKey
    private long mId;
    private long sendingDate;
    private String message;
    private int scheduleStatus;
    private String Title;
    private RealmList<RealmContact> mContacts;

    public RealmSchedule(long id,
                         String title,
                         long sendingDate,
                         String message,
                         int scheduleStatus,
                         RealmList<RealmContact> contacts) {
        mId = id;
        setTitle(title);
        setSendingDate(sendingDate);
        setMessage(message);
        setScheduleStatus(scheduleStatus);
        setContacts(contacts);
    }

    public RealmSchedule() {
    }

    public RealmList<RealmContact> getContacts() {
        return mContacts;
    }

    public void setContacts(RealmList<RealmContact> contacts) {
        mContacts = contacts;
    }

    public long getId() {
        return mId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getScheduleStatus() {
        return scheduleStatus;
    }

    public void setScheduleStatus(int scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    public long getSendingDate() {
        return sendingDate;
    }

    public void setSendingDate(long sendingDate) {
        this.sendingDate = sendingDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
