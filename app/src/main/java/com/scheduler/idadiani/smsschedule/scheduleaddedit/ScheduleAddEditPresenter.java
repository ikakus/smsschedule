package com.scheduler.idadiani.smsschedule.scheduleaddedit;

import com.scheduler.idadiani.smsschedule.enums.Error;
import com.scheduler.idadiani.smsschedule.enums.ScheduleStatus;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryImpl;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryInterface;
import com.scheduler.idadiani.smsschedule.mvp.presentation.Presenter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by idadiani on 13/09/16.
 */
public class ScheduleAddEditPresenter implements Presenter<ScheduleAddEditView> {
    public static final String TIME_PATTERN = "hh:mm aa";
    private ScheduleModel mScheduleModel;
    private RepositoryInterface mRepository;
    private ScheduleAddEditView mView;
    private List<ContactModel> mContactModels;
    private Calendar mCalendarDate;

    public ScheduleAddEditPresenter() {
        mRepository = new RepositoryImpl();
        mCalendarDate = Calendar.getInstance();
        mContactModels = new ArrayList<>();
    }

    public List<ContactModel> getContactModels() {
        return mContactModels;
    }

    public void setContactModels(List<ContactModel> contactModels) {
        mContactModels = contactModels;
        mView.fillContactsField(mContactModels);
    }

    public Calendar getCalendarDate() {
        return mCalendarDate;
    }

    public void setCalendarDate(Calendar calendarDate) {
        mCalendarDate = calendarDate;
        DateFormat simpleDateFormat = SimpleDateFormat.getDateInstance();
        String date = simpleDateFormat.format(calendarDate.getTime());
        mView.setDate(date);
    }

    public void setCalendarTime(Calendar calendarDate) {
        mCalendarDate = calendarDate;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIME_PATTERN);
        String date = simpleDateFormat.format(calendarDate.getTime());
        mView.setTime(date);
    }

    public ScheduleModel getScheduleModel() {
        return mScheduleModel;
    }

    @Override
    public void setView(ScheduleAddEditView view) {
        mView = view;
    }

    public void addSchedule(ScheduleModel scheduleModel) {
        if (validate(scheduleModel)) {
            scheduleModel.setScheduleStatus(ScheduleStatus.IN_QUEUE);
            mRepository.addSchedule(scheduleModel);
            mView.setAlarm(scheduleModel);
            mView.close();
        }
    }

    private boolean validate(ScheduleModel scheduleModel) {
        boolean result = true;
        if (scheduleModel.getSendingDate().compareTo(Calendar.getInstance()) < 0) {
            mView.showError(Error.SET_DATE);
            result = false;
        }

        if (scheduleModel.getContactModels().size() == 0) {
            mView.showError(Error.SET_CONTACTS);
            result = false;
        }

        if (scheduleModel.getMessage().length() == 0) {
            mView.showError(Error.SET_MESSAGE);
            result = false;
        }
        return result;
    }

    public void loadSchedule(long scheduleId) {
        if (scheduleId == -1) {
            Calendar calendarDate = Calendar.getInstance();
            mScheduleModel = new ScheduleModel(calendarDate, "");
            mScheduleModel.setScheduleStatus(ScheduleStatus.PENDING);
            mView.setMode(ScheduleViewMode.CREATE);
        } else {
            mScheduleModel = mRepository.getScheduleById(scheduleId);
            mView.setMode(ScheduleViewMode.EDIT);
        }

        mView.fillScheduleData(mScheduleModel);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    public void onDateClick() {
        mView.showDatePicker();
    }

    public void onTimeClick() {
        mView.showTimePicker();
    }

    public void onContactsClick() {
        mView.showContactsSelector();
    }

    public void onDoneClicked(String title, String message) {
        mScheduleModel.setTitle(title);
        mScheduleModel.setMessage(message);
        mScheduleModel.setSendingDate(mCalendarDate);
        mScheduleModel.setContactModels(mContactModels);
        addSchedule(mScheduleModel);
    }
}
