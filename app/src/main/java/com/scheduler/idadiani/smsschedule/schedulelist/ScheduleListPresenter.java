package com.scheduler.idadiani.smsschedule.schedulelist;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryImpl;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryInterface;
import com.scheduler.idadiani.smsschedule.mvp.presentation.Presenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idadiani on 15/09/16.
 */
public class ScheduleListPresenter implements Presenter<ScheduleListView> {
    private ScheduleListView mView;
    private RepositoryInterface mRepository;
    private List<ScheduleModel> mSchedules;

    public ScheduleListPresenter() {
        mRepository = new RepositoryImpl();
        mSchedules = new ArrayList<>();
    }

    private void loadSchedules() {
        mSchedules = mRepository.getAllSchedules();
    }

    public void startNewSchedule() {
        mView.startNewScheduleCreation();
    }

    public void showHistory() {
        mView.showHistory();
    }

    public void deleteSchedule(ScheduleModel scheduleModel) {
        mRepository.deleteSchedule(scheduleModel);
        loadSchedules();
        mView.populateList(mSchedules);

    }

    public void openScheduleDetailed(ScheduleModel scheduleModel) {
        mView.showDetailed(scheduleModel);
    }

    @Override
    public void setView(ScheduleListView view) {
        mView = view;
    }

    @Override
    public void resume() {
        loadSchedules();
        mView.populateList(mSchedules);
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
