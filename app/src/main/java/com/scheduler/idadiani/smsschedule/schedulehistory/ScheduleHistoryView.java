package com.scheduler.idadiani.smsschedule.schedulehistory;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

import java.util.List;

/**
 * Created by idadiani on 07/10/16.
 */

public interface ScheduleHistoryView {
    void populateList(List<ScheduleModel> schedules);
}
