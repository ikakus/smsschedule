package com.scheduler.idadiani.smsschedule.scheduledetails;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

/**
 * Created by idadiani on 07/10/16.
 */

public interface ScheduleDetailsView {
    void fillScheduleData(ScheduleModel scheduleModel);
    void startScheduleEdit(ScheduleModel scheduleModel);
}
