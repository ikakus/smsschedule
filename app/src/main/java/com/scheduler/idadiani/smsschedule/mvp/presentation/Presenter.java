package com.scheduler.idadiani.smsschedule.mvp.presentation;

/**
 * Created by idadiani on 13/09/16.
 */
public interface Presenter<T> extends BasePresenter{
    void setView(T view);
}
