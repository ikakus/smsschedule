package com.scheduler.idadiani.smsschedule.scheduledetails;

import com.scheduler.idadiani.smsschedule.enums.ScheduleStatus;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryImpl;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryInterface;
import com.scheduler.idadiani.smsschedule.mvp.presentation.Presenter;

import java.util.Calendar;

/**
 * Created by idadiani on 07/10/16.
 */

public class ScheduleDetailsPresenter implements Presenter<ScheduleDetailsView> {
    private ScheduleModel mScheduleModel;
    private ScheduleDetailsView mView;
    private RepositoryInterface mRepository;
    public ScheduleDetailsPresenter() {
        mRepository = new RepositoryImpl();
    }

    public void loadSchedule(long scheduleId) {
        if (scheduleId == -1) {
            Calendar calendarDate = Calendar.getInstance();
            mScheduleModel = new ScheduleModel(calendarDate, "");
            mScheduleModel.setScheduleStatus(ScheduleStatus.PENDING);

        } else {
            mScheduleModel = mRepository.getScheduleById(scheduleId);
        }
        mView.fillScheduleData(mScheduleModel);
    }

    public void startEdit() {
        mView.startScheduleEdit(mScheduleModel);
    }

    @Override
    public void setView(ScheduleDetailsView view) {
        mView = view;
    }

    @Override
    public void resume() {
        loadSchedule(mScheduleModel.getId());
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }


}
