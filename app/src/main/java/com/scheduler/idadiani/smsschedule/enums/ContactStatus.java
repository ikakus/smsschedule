package com.scheduler.idadiani.smsschedule.enums;

/**
 * Created by i.dadiani on 4/24/2015.
 */
public enum ContactStatus {
    PENDING(0),
    SENT(1),
    DELIVERED(2);

    int value;
    ContactStatus(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

    public static ContactStatus get(int i){
        if(i == 0){
            return PENDING;
        }else if(i == 1){
            return SENT;
        }else if (i==2){
            return DELIVERED;
        }
        return null;
    }
}
