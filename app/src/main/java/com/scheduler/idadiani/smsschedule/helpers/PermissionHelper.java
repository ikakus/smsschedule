package com.scheduler.idadiani.smsschedule.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by ikakus on 10/14/16.
 */

public class PermissionHelper {
    public static final int PERMISSIONS = 101;

    private static void requestPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_CONTACTS}, PERMISSIONS);
    }

    private static boolean checkPermission(Activity activity) {
        return ActivityCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED;
    }

    public  static void processPermissions(Activity activity){
        if (PermissionHelper.checkPermission(activity)) {
            PermissionHelper.requestPermission(activity);
            return;
        }
    }
}
