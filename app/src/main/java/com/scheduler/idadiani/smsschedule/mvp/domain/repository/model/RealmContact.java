package com.scheduler.idadiani.smsschedule.mvp.domain.repository.model;

import io.realm.RealmObject;

/**
 * Created by i.dadiani on 4/8/2015.
 */
public class RealmContact extends RealmObject {

    private long mId;
    private int mContactStatus;
    private String mPhoneNumber;
    private String mName;

    public RealmContact() {

    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.mPhoneNumber = phoneNumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getContactStatus() {
        return mContactStatus;
    }

    public void setContactStatus(int contactStatus) {
        this.mContactStatus = contactStatus;
    }


    public long getId() {
        return mId;
    }
    public void setId(long id) {
        this.mId = id;
    }

    public RealmContact(long id, String name, String phoneNumber, int contactStatus) {
        setName(name);
        setPhoneNumber(phoneNumber);
        setId(id);
        setContactStatus(contactStatus);
    }

    @Override
    public boolean equals(Object o) {
        if(this.getName().equals(((RealmContact)o).getName())){
            if(this.getPhoneNumber().equals(((RealmContact)o).getPhoneNumber())){
                return true;
            }
        }
        return false;
    }

}
