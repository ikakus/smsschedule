package com.scheduler.idadiani.smsschedule.scheduleaddedit;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

import java.util.List;

/**
 * Created by idadiani on 13/09/16.
 */
public interface ScheduleAddEditView {
    void fillScheduleData(ScheduleModel scheduleModel);
    void showError(int errorMessageCode);
    void setMode(int mode);
    void setAlarm(ScheduleModel scheduleModel);
    void fillContactsField(List<ContactModel> contactModels);
    void showContactsSelector();
    void showDatePicker();
    void showTimePicker();
    void setDate(String date);
    void setTime(String date);
    void close();
}
