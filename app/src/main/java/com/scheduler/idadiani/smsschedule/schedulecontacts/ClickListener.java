package com.scheduler.idadiani.smsschedule.schedulecontacts;

/**
 * Created by idadiani on 11/10/16.
 */

public interface ClickListener {
    void onClick(int position);
}
