package com.scheduler.idadiani.smsschedule.mvp.domain.model;

import com.scheduler.idadiani.smsschedule.enums.ScheduleStatus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import io.realm.annotations.PrimaryKey;

/**
 * Created by i.dadiani on 4/8/2015.
 */
public class ScheduleModel {
    @PrimaryKey
    private long mId;
    private Calendar mSendingDate;
    private String mMessage;
    private ScheduleStatus mScheduleStatus;
    private String mTitle;
    private List<ContactModel> mContactModels;

    public ScheduleModel() {
        mId = UUID.randomUUID().hashCode();
    }

    public ScheduleModel(long id,
                         String title,
                         Calendar date,
                         String message,
                         ScheduleStatus scheduleStatus,
                         List<ContactModel> contacts) {
        mId = id;
        setTitle(title);
        setSendingDate(date);
        setMessage(message);
        setScheduleStatus(scheduleStatus);
        setContactModels(contacts);
    }

    public ScheduleModel(String title, Calendar date, String message) {
        this();
        setTitle(title);
        setSendingDate(date);
        setMessage(message);
        setScheduleStatus(ScheduleStatus.PENDING);
    }

    public ScheduleModel(Calendar date, String message) {
        this();
        setTitle("Untitled");
        setSendingDate(date);
        setMessage(message);
        setScheduleStatus(ScheduleStatus.PENDING);
        mContactModels = new ArrayList<>();
        mSendingDate = Calendar.getInstance();
    }

    public long getId() {
        return mId;
    }

    public List<ContactModel> getContactModels() {
        return mContactModels;
    }

    public void setContactModels(List<ContactModel> contactModels) {
        mContactModels = contactModels;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public ScheduleStatus getScheduleStatus() {
        return mScheduleStatus;
    }

    public void setScheduleStatus(ScheduleStatus scheduleStatus) {
        this.mScheduleStatus = scheduleStatus;
    }

    public Calendar getSendingDate() {
        return mSendingDate;
    }

    public void setSendingDate(Calendar sendingDate) {
        this.mSendingDate = sendingDate;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

}
