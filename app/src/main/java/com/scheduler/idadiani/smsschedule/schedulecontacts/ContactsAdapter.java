package com.scheduler.idadiani.smsschedule.schedulecontacts;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i.dadiani on 4/20/2015.
 */

public class ContactsAdapter extends ArrayAdapter<ContactModel>{
    private Activity mActivity;
    private List<ContactModel> mContactModels = new ArrayList<>();
    private ClickListener mClickListener;

    public ContactsAdapter(Activity context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.mActivity = context;
    }

    public void setClickListener(ClickListener clickListener) {
        mClickListener = clickListener;
    }

    public void setContacts(List<ContactModel> mItems) {
        this.mContactModels = mItems;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mContactModels.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        ContactModel phoneContactModel = mContactModels.get(position);
        if (convertView == null) {

            LayoutInflater mInflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_contact, null);

            viewHolder = new ViewHolder(convertView);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String phoneNumber = phoneContactModel.getPhoneNumber();
        if (phoneNumber != null) {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            String shortName = phoneContactModel.getName().substring(0, 1);
            int color2 = generator.getColor(shortName);
            TextDrawable drawable2 = TextDrawable.builder().buildRound(shortName, color2);

            viewHolder.textViewContactName.setText(phoneContactModel.getName());
            viewHolder.textViewContactNumber.setText(phoneNumber);
            viewHolder.imageView.setImageDrawable(drawable2);
            convertView.setTag(viewHolder);

        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(position);
            }
        });

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public static class ViewHolder {
        public final TextView textViewContactName;
        public final TextView textViewContactNumber;
        public final ImageView imageView;

        public ViewHolder(View view) {
            textViewContactName = (TextView) view.findViewById(R.id.contact_name);
            textViewContactNumber = (TextView) view.findViewById(R.id.contact_number);
            imageView = (ImageView) view.findViewById(R.id.icon);
        }
    }

}