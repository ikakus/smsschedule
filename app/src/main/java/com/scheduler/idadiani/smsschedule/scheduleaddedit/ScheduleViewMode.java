package com.scheduler.idadiani.smsschedule.scheduleaddedit;

/**
 * Created by idadiani on 15/09/16.
 */
public final class ScheduleViewMode {
   public static int CREATE = 0;
   public static int EDIT = 1;
}
