package com.scheduler.idadiani.smsschedule.schedulelist;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;

import com.daimajia.swipe.util.Attributes;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.helpers.PermissionHelper;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.scheduleaddedit.ActivityAddEditSchedule;
import com.scheduler.idadiani.smsschedule.scheduledetails.ActivityScheduleDetails;
import com.scheduler.idadiani.smsschedule.schedulehistory.ActivityScheduleHistoryList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityScheduleList extends AppCompatActivity implements ScheduleListView, ScheduleActionListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(android.R.id.list)
    DynamicListView mSchedulesListView;
    @BindView(R.id.fab)
    FloatingActionButton mFab;

    private ScheduleSwipeAdapter mSchedulesAdapter;
    private ScheduleListPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_list);
        ButterKnife.bind(this);

        mSchedulesAdapter = new ScheduleSwipeAdapter(this);
        mSchedulesAdapter.setMode(Attributes.Mode.Single);
        mSchedulesAdapter.setActionListener(this);

        ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(mSchedulesAdapter);
        mSchedulesListView.setAdapter(animationAdapter);

        mPresenter = new ScheduleListPresenter();
        mPresenter.setView(this);
        setToolbar();
        setListeners();
        PermissionHelper.processPermissions(this);

    }

    private void setListeners() {
        mSchedulesListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                mSchedulesAdapter.closeAllItems();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
    }

    public void fillList(List<ScheduleModel> scheduleModels) {
        mSchedulesAdapter.setItems(scheduleModels);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_schedule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_history) {
            mPresenter.showHistory();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionHelper.PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    mFab.hide();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

                if (grantResults.length > 0
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    mFab.hide();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void populateList(List<ScheduleModel> list) {
        fillList(list);
    }

    @Override
    public void startNewScheduleCreation() {
        Intent newScheduleIntent = new Intent(this, ActivityAddEditSchedule.class);
        this.startActivity(newScheduleIntent);
    }

    @Override
    public void showHistory() {
        Intent intent = new Intent(this, ActivityScheduleHistoryList.class);
        this.startActivity(intent);
    }

    @Override
    public void showDetailed(ScheduleModel scheduleModel) {
        Intent scheduleEditIntent = new Intent(this, ActivityScheduleDetails.class);
        scheduleEditIntent.putExtra(ActivityScheduleDetails.SCHEDULE_ID, scheduleModel.getId());
        this.startActivity(scheduleEditIntent);
    }

    @Override
    public void detailed(ScheduleModel scheduleModel) {
        mPresenter.openScheduleDetailed(scheduleModel);
    }

    @Override
    public void delete(ScheduleModel scheduleModel) {
        mPresenter.deleteSchedule(scheduleModel);
    }

    @OnClick(R.id.fab)
    public void onClick() {
        mPresenter.startNewSchedule();
    }
}