package com.scheduler.idadiani.smsschedule.schedulelist;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.nhaarman.listviewanimations.util.Insertable;
import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by i.dadiani on 5/21/2015.
 */
public class ScheduleSwipeAdapter extends BaseSwipeAdapter implements Insertable {

    private static final String TAG = "ScheduleSwipeAdapter";
    private Context mContext;
    private List<ScheduleModel> mScheduleModels;
    private ScheduleActionListener mActionListener;

    public ScheduleSwipeAdapter(Context mContext) {
        this.mContext = mContext;
        this.mScheduleModels = new ArrayList<>();
    }

    public void setItems(List<ScheduleModel> items){
        mScheduleModels = items;
        notifyDataSetChanged();
    }

    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int position, ViewGroup viewGroup) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_schedule_swipe, null);
        SwipeLayout swipeLayout = (SwipeLayout) convertView.findViewById(getSwipeLayoutResourceId(position));

        swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeLayout.findViewById(R.id.delete));

        convertView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteDialog(position);
            }
        });

        convertView.findViewById(R.id.item_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startEditScheduleAtPosition(position);

            }
        });
        return convertView;
    }

    private void showDeleteDialog(final int position) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(mContext, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(mContext.getString(R.string.delete_warning));
        builder.setMessage(mContext.getString(R.string.delete_confirmation));
        builder.setCancelable(false);
        builder.setPositiveButton(mContext.getString(R.string.delete_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteScheduleAtPosition(position);
            }
        });
        builder.setNegativeButton(mContext.getString(R.string.delete_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                closeAllItems();
            }
        });
        builder.show();
    }

    private void startEditScheduleAtPosition(int position) {
        ScheduleModel scheduleModel = mScheduleModels.get(position);
        if(mActionListener !=null) {
            mActionListener.detailed(scheduleModel);
            closeAllItems();
        }else {
            Log.e(TAG, "Listener not set");
        }
    }

    private void deleteScheduleAtPosition(int position) {
        ScheduleModel scheduleModel = mScheduleModels.get(position);
        deleteSchedule(scheduleModel);
    }

    private void deleteSchedule(ScheduleModel scheduleModel) {
        if(mActionListener !=null) {
            mActionListener.delete(scheduleModel);
            closeAllItems();
            notifyDataSetChanged();
        }else {
            Log.e(TAG, "Listener not set");
        }
    }

    @Override
    public void fillValues(int position, View convertView) {
        if (convertView == null) {
            LayoutInflater mInflater =
                    (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_schedule_swipe, null);
        }
        final ScheduleModel scheduleModel = mScheduleModels.get(position);

        if (scheduleModel != null) {
            TextView textViewDate = (TextView) convertView.findViewById(R.id.schedule_date);
            TextView textViewMessage = (TextView) convertView.findViewById(R.id.schedule_message);
            TextView textViewTitle = (TextView) convertView.findViewById(R.id.title);

            ImageView imageView = (ImageView) convertView.findViewById(R.id.icon);

            final DateFormat DateFormat = android.text.format.DateFormat
                    .getMediumDateFormat(mContext.getApplicationContext());
            String pattern = ((SimpleDateFormat) DateFormat).toPattern();
            SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
            Calendar calendar = scheduleModel.getSendingDate();

            String date = format.format(calendar.getTime());
            textViewDate.setText(date);
            textViewMessage.setText(scheduleModel.getMessage());
            textViewTitle.setText(scheduleModel.getTitle());

            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color2 = generator.getColor(Integer.toString(calendar.get(Calendar.MONTH)));

            TextDrawable drawable2 = TextDrawable.builder()
                    .buildRound(Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)), color2);

            imageView.setImageDrawable(drawable2);
        }
    }

    @Override
    public int getCount() {
        return mScheduleModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void add(int i, @NonNull Object o) {

    }

    public void setActionListener(ActivityScheduleList actionListener) {
        mActionListener = actionListener;
    }
}
