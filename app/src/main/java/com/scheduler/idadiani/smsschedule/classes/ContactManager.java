package com.scheduler.idadiani.smsschedule.classes;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.scheduler.idadiani.smsschedule.SmsScheduleApplication;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i.dadiani on 4/20/2015.
 */
public class ContactManager {

    private static final String[] PROJECTION = new String[]{
            ContactsContract.CommonDataKinds.Email.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Email.DATA
    };

    public static List<ContactModel> getContactsFast() {
        // not very good idea I guess...
        ContentResolver cr = SmsScheduleApplication.getInstance().getContentResolver();

        List<ContactModel> contactModels = new ArrayList<>();
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION, null, null, null);
        if (cursor != null) {
            try {
                final int contactIdIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID);
                final int displayNameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                final int phoneNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                long contactId;
                String displayName, phone;
                while (cursor.moveToNext()) {
                    contactId = cursor.getLong(contactIdIndex);
                    displayName = cursor.getString(displayNameIndex);
                    phone = cursor.getString(phoneNumber);

                    contactModels.add(new ContactModel(contactId, displayName, phone));

                }
            } finally {
                cursor.close();
            }
        }
        return contactModels;
    }
}
