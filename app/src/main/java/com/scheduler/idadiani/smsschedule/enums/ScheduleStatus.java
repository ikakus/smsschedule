package com.scheduler.idadiani.smsschedule.enums;

/**
 * Created by i.dadiani on 4/24/2015.
 */

public enum ScheduleStatus {
    PENDING(0),
    IN_QUEUE(1),
    SENT(2);

    int value;
    ScheduleStatus(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

    public static ScheduleStatus get(int i){
        if(i == 0){
            return PENDING;
        }else if(i == 1){
            return IN_QUEUE;
        }else if (i==2){
            return SENT;
        }
        return null;
    }
}
