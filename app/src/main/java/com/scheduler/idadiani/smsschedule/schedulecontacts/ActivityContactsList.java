package com.scheduler.idadiani.smsschedule.schedulecontacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityContactsList extends AppCompatActivity implements ContactsView, ClickListener {

    public static final String CONTACTS = "contacts";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.search_edit_text)
    EditText mSearchEditText;
    @BindView(R.id.list)
    ListView mContactsListView;
    private ContactsAdapter mContactsAdapter;
    private ContactsPresenter mPresenter;

    private TextWatcher searchQueryWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mPresenter.search(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);
        ButterKnife.bind(this);
        mPresenter = new ContactsPresenter();
        mPresenter.setView(this);
        mSearchEditText.addTextChangedListener(searchQueryWatcher);

        setToolbar();

        mContactsAdapter = new ContactsAdapter(this, R.layout.item_contact);
        mContactsAdapter.setClickListener(this);
        mContactsListView.setAdapter(mContactsAdapter);

    }

    private void setToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(R.string.contact_list_title);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.resume();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            // load selected contacts when editing
            ArrayList<ContactModel> contactModels = (ArrayList<ContactModel>) extras.getSerializable(CONTACTS);
            mPresenter.loadContacts(contactModels);
        }
    }

    @Override
    public void populateList(List<ContactModel> contactModels) {
        fillList(contactModels);

    }

    @Override
    public void setItemChecked(int id, boolean b) {
        mContactsListView.setItemChecked(id, b);
    }

    @Override
    public void returnSelectedContacts(List<ContactModel> selectedItems) {
        ArrayList<ContactModel> contactModelArrayList = new ArrayList<>(selectedItems);
        Intent returnIntent = new Intent();
        returnIntent.putExtra(CONTACTS, contactModelArrayList);
        setResult(RESULT_OK, returnIntent);
        this.finish();
    }

    private void fillList(List<ContactModel> contactModels) {
        mContactsAdapter.setContacts(contactModels);
    }

    private void doneAction() {
        mPresenter.done();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.button_done)
    public void onClick() {
        doneAction();
    }

    @Override
    public void onClick(int position) {
        mPresenter.itemClicked(position);
    }
}
