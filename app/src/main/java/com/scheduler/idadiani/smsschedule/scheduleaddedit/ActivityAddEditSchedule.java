package com.scheduler.idadiani.smsschedule.scheduleaddedit;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;
import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.classes.AlarmReceiver;
import com.scheduler.idadiani.smsschedule.helpers.ErrorHelper;
import com.scheduler.idadiani.smsschedule.helpers.PermissionHelper;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.schedulecontacts.ActivityContactsList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityAddEditSchedule extends AppCompatActivity implements ScheduleAddEditView {

    public static final String SCHEDULE_ID = "schedule_id";
    public static final String CONTACTS = "contacts";
    @BindView(R.id.title)
    EditText mTitle;
    @BindView(R.id.schedule_date)
    EditText mScheduleDate;
    @BindView(R.id.time)
    EditText mTime;
    @BindView(R.id.message)
    EditText mMessage;
    private int mContactsRequestCode = 1;
    private ScheduleAddEditPresenter mPresenter;
    private int PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_edit);
        ButterKnife.bind(this);
        setToolBar();
        Intent intent = getIntent();
        long scheduleID = intent.getLongExtra(SCHEDULE_ID, -1);

        mPresenter = new ScheduleAddEditPresenter();
        mPresenter.setView(this);
        mPresenter.loadSchedule(scheduleID);

    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void loadValues(ScheduleModel scheduleModel) {
        Calendar calendar = scheduleModel.getSendingDate();
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) SimpleDateFormat.getDateInstance();

        mMessage.setText(scheduleModel.getMessage());
        mScheduleDate.setText(simpleDateFormat.format(calendar.getTime()));
        mTime.setText(simpleTimeFormat.format(calendar.getTime()));
        mTitle.setText(scheduleModel.getTitle());

        List<ContactModel> contactModels = scheduleModel.getContactModels();
        fillContactsField(contactModels);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == mContactsRequestCode) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                mPresenter.setContactModels((List<ContactModel>) extras.getSerializable(CONTACTS));
            }
        }
    }

    @Override
    public void fillContactsField(List<ContactModel> contactModels) {
        EditText mContactsEditText = (EditText) findViewById(R.id.contacts);
        mContactsEditText.setText("");
        for (ContactModel contactModel : contactModels) {
            mContactsEditText.append(contactModel.getName() + " ");
        }
    }

    @Override
    public void showContactsSelector() {

        PermissionHelper.processPermissions(this);

        Intent contactsIntent = new Intent(this, ActivityContactsList.class);
        ArrayList<ContactModel> contactModels = (ArrayList) mPresenter.getContactModels();
        contactsIntent.putExtra(CONTACTS, contactModels);
        this.startActivityForResult(contactsIntent, mContactsRequestCode);
    }

    @Override
    public void showDatePicker() {

        Dialog.Builder builder;
        builder = new DatePickerDialog.Builder() {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog) fragment.getDialog();
                mPresenter.setCalendarDate(dialog.getCalendar());
                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        builder.positiveAction(getString(R.string.add_schedule_ok))
                .negativeAction(getString(R.string.add_schedule_cancel));

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);

    }

    @Override
    public void showTimePicker() {
        Dialog.Builder builder;

        Calendar rightNow = mPresenter.getCalendarDate();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);
        int minute = rightNow.get(Calendar.MINUTE);

        builder = new TimePickerDialog.Builder(hour, minute) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                TimePickerDialog dialog = (TimePickerDialog) fragment.getDialog();

                super.onPositiveActionClicked(fragment);
                Calendar calendar = mPresenter.getCalendarDate();
                calendar.set(Calendar.HOUR_OF_DAY, dialog.getHour());
                calendar.set(Calendar.MINUTE, dialog.getMinute());
                mPresenter.setCalendarTime(calendar);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                Toast.makeText(fragment.getDialog().getContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                super.onNegativeActionClicked(fragment);
            }
        };

        builder.positiveAction(getString(R.string.add_schedule_ok))
                .negativeAction(getString(R.string.add_schedule_cancel));

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);

    }

    @Override
    public void setDate(String date) {
        mScheduleDate.setText(date);
    }

    @Override
    public void setTime(String date) {
        mTime.setText(date);
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void fillScheduleData(ScheduleModel scheduleModel) {
        loadValues(scheduleModel);
    }

    @Override
    public void showError(int errorCode) {
        String message = ErrorHelper.getMessage(this, errorCode);
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }

    @Override
    public void setMode(int mode) {
        int res = 0;
        if (mode == ScheduleViewMode.CREATE) {
            res = R.string.new_schedule;
        } else if (mode == ScheduleViewMode.EDIT) {
            res = R.string.edit_schedule;
        }
        if (res != 0) {
            getSupportActionBar().setTitle(res);
        }
    }

    @Override
    public void setAlarm(ScheduleModel scheduleModel) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intentAlarm = new Intent(this, AlarmReceiver.class);
        intentAlarm.putExtra("schedule_id", scheduleModel.getId());
        alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                scheduleModel.getSendingDate().getTimeInMillis(),
                PendingIntent.getBroadcast(this,
                        ((int) scheduleModel.getId()),
                        intentAlarm,
                        PendingIntent.FLAG_UPDATE_CURRENT));
    }

    @OnClick({R.id.schedule_date, R.id.time, R.id.contacts})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.schedule_date:
                mPresenter.onDateClick();
                break;
            case R.id.time:
                mPresenter.onTimeClick();
                break;
            case R.id.contacts:
                mPresenter.onContactsClick();
                break;
        }
    }

    @OnClick(R.id.button_done)
    public void onClick() {
        String message = mMessage.getText().toString();
        String title = mTitle.getText().toString();
        mPresenter.onDoneClicked(title, message);
    }
}