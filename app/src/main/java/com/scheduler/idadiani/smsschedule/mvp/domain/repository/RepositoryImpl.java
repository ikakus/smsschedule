package com.scheduler.idadiani.smsschedule.mvp.domain.repository;

import com.scheduler.idadiani.smsschedule.enums.ScheduleStatus;
import com.scheduler.idadiani.smsschedule.helpers.Converter;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.model.RealmSchedule;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by idadiani on 13/09/16.
 */
public class RepositoryImpl implements RepositoryInterface {
    @Override
    public void addSchedule(ScheduleModel scheduleModel) {
        RealmSchedule realmSchedule = Converter.toRealmSchedule(scheduleModel);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Realm.getDefaultInstance().copyToRealmOrUpdate(realmSchedule);
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void deleteSchedule(ScheduleModel scheduleModel) {
        RealmSchedule dbModel = Converter.toRealmSchedule(scheduleModel);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.where(RealmSchedule.class)
                .equalTo("mId", dbModel.getId())
                .findAll()
                .deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void updateSchedule(ScheduleModel scheduleModel) {
        RealmSchedule realmSchedule = Converter.toRealmSchedule(scheduleModel);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Realm.getDefaultInstance().copyToRealmOrUpdate(realmSchedule);
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public ScheduleModel getScheduleById(long id) {
        Realm realm = Realm.getDefaultInstance();
        RealmSchedule realmSchedule =  realm.where(RealmSchedule.class).equalTo("mId", id).findFirst();
        realm.close();
        return Converter.toScheduleModel(realmSchedule);
    }

    @Override
    public List<ScheduleModel> getAllSchedules() {

        Realm realm = Realm.getDefaultInstance();
        RealmResults<RealmSchedule> list = realm.where(RealmSchedule.class)
                .equalTo("scheduleStatus", ScheduleStatus.IN_QUEUE.getValue())
                .findAll();
        RealmList <RealmSchedule> finalList = new RealmList<RealmSchedule>();
        finalList.addAll(list.subList(0, list.size()));
        List<ScheduleModel> convertedList = Converter.toScheduleModel(finalList);
        realm.close();
        return convertedList;
    }

    @Override
    public List<ScheduleModel> getArchivedSchedules() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<RealmSchedule> list = realm.where(RealmSchedule.class)
                .equalTo("scheduleStatus", ScheduleStatus.SENT.getValue())
                .findAll();
        RealmList <RealmSchedule> finalList = new RealmList<RealmSchedule>();
        finalList.addAll(list.subList(0, list.size()));
        realm.close();
        return Converter.toScheduleModel(finalList);
    }
}
