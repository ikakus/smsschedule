package com.scheduler.idadiani.smsschedule;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by idadiani on 16/09/16.
 */
public class SmsScheduleApplication extends Application {
    private static SmsScheduleApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Realm.setDefaultConfiguration(
                new RealmConfiguration.Builder(this)
                        .deleteRealmIfMigrationNeeded()
                        .build());

    }

    public static SmsScheduleApplication getInstance() {
        return mInstance;
    }


}
