package com.scheduler.idadiani.smsschedule.schedulehistory;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryImpl;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryInterface;
import com.scheduler.idadiani.smsschedule.mvp.presentation.Presenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idadiani on 07/10/16.
 */

public class ScheduleHistoryPresenter implements Presenter<ScheduleHistoryView> {
    ScheduleHistoryView mView;
    private RepositoryInterface mRepository;
    private List<ScheduleModel> mSchedules;

    public ScheduleHistoryPresenter() {
        mRepository = new RepositoryImpl();
        mSchedules = new ArrayList<>();
    }

    @Override
    public void setView(ScheduleHistoryView view) {
        mView = view;
    }

    private void loadSchedules() {
        mSchedules = mRepository.getArchivedSchedules();
    }

    @Override
    public void resume() {
        loadSchedules();
        mView.populateList(mSchedules);
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
