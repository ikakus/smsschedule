package com.scheduler.idadiani.smsschedule.scheduledetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;

import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.scheduleaddedit.ActivityAddEditSchedule;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityScheduleDetails extends AppCompatActivity implements ScheduleDetailsView {

    public static final String SCHEDULE_ID = "schedule_id";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout mToolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout mAppBar;
    @BindView(R.id.schedule_date)
    EditText mScheduleDate;
    @BindView(R.id.time)
    EditText mTime;
    @BindView(R.id.contacts)
    EditText mContacts;
    @BindView(R.id.message)
    EditText mMessage;
    private ScheduleDetailsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_schedule_details);
        ButterKnife.bind(this);
        setToolBar();
        Intent intent = getIntent();
        long scheduleID = intent.getLongExtra(SCHEDULE_ID, -1);

        mPresenter = new ScheduleDetailsPresenter();
        mPresenter.setView(this);
        mPresenter.loadSchedule(scheduleID);
        setInactive();
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setInactive() {
        mContacts.setEnabled(false);
        mScheduleDate.setEnabled(false);
        mTime.setEnabled(false);
        mMessage.setEnabled(false);

        mContacts.setFocusableInTouchMode(false);
        mScheduleDate.setFocusableInTouchMode(false);
        mTime.setFocusableInTouchMode(false);
        mMessage.setFocusableInTouchMode(false);
    }

    @Override
    public void fillScheduleData(ScheduleModel scheduleModel) {
        Calendar calendar = scheduleModel.getSendingDate();
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) SimpleDateFormat.getDateInstance();
        List<ContactModel> contactModels = scheduleModel.getContactModels();

        mContacts.setText("");
        for (ContactModel contactModel : contactModels) {
            mContacts.append(contactModel.getName() + " ");
        }

        mMessage.setText(scheduleModel.getMessage());
        mScheduleDate.setText(simpleDateFormat.format(calendar.getTime()));
        mTime.setText(simpleTimeFormat.format(calendar.getTime()));
        mToolbar.setTitle(scheduleModel.getTitle());
        mToolbarLayout.setTitle(scheduleModel.getTitle());
    }

    @Override
    public void startScheduleEdit(ScheduleModel scheduleModel) {
        Intent scheduleEditIntent = new Intent(this, ActivityAddEditSchedule.class);
        scheduleEditIntent.putExtra(SCHEDULE_ID, scheduleModel.getId());
        this.startActivity(scheduleEditIntent);
    }

    @OnClick(R.id.edit_button)
    public void onClick() {
        mPresenter.startEdit();
    }
}
