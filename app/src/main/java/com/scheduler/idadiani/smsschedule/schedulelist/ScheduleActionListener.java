package com.scheduler.idadiani.smsschedule.schedulelist;

import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;

/**
 * Created by idadiani on 15/09/16.
 */
public interface ScheduleActionListener {
    void detailed(ScheduleModel scheduleModel);
    void delete(ScheduleModel scheduleModel);
}
