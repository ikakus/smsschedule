package com.scheduler.idadiani.smsschedule.helpers;

import com.scheduler.idadiani.smsschedule.enums.ContactStatus;
import com.scheduler.idadiani.smsschedule.enums.ScheduleStatus;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.model.RealmContact;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.model.RealmSchedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by idadiani on 14/09/16.
 */
public class Converter {

    public static ScheduleModel toScheduleModel(RealmSchedule realmSchedule){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(realmSchedule.getSendingDate());

        return new ScheduleModel(
                realmSchedule.getId(),
                realmSchedule.getTitle(),
                cal,
                realmSchedule.getMessage(),
                ScheduleStatus.get(realmSchedule.getScheduleStatus()),
                toContactModel(realmSchedule.getContacts())
        );
    }

    public static List<ScheduleModel> toScheduleModel(RealmList<RealmSchedule> list){
        List<ScheduleModel> modelsList = new ArrayList<>();
        for (RealmSchedule realmSchedule : list) {
            modelsList.add(Converter.toScheduleModel(realmSchedule));
        }
        return modelsList;
    }

    public static List<ContactModel> toContactModel(RealmList<RealmContact> list){
        List<ContactModel> modelsList = new ArrayList<>();
        for (RealmContact realmSchedule : list) {
            modelsList.add(Converter.toContactModel(realmSchedule));
        }
        return modelsList;
    }

    public static ContactModel toContactModel(RealmContact realmContact){
        int status = realmContact.getContactStatus();
        ContactStatus contactStatus = ContactStatus.get(status);
        return new ContactModel(
                realmContact.getId(),
                realmContact.getName(),
                realmContact.getPhoneNumber(),
                contactStatus
        );
    }

    public static RealmList<RealmContact> toRealmContacts(List<ContactModel> contactModels){
        RealmList<RealmContact> realmContacts = new RealmList<>();
        for (ContactModel contactModel : contactModels) {
            realmContacts.add(toRealmContact(contactModel));
        }
        return realmContacts;
    }

    public static RealmContact toRealmContact(ContactModel contactModel){
        return new RealmContact(
                contactModel.getId(),
                contactModel.getName(),
                contactModel.getPhoneNumber(),
                contactModel.getContactStatus().getValue()
        );
    }

    public static RealmSchedule toRealmSchedule(ScheduleModel scheduleModel){
        return new RealmSchedule(
                scheduleModel.getId(),
                scheduleModel.getTitle(),
                scheduleModel.getSendingDate().getTimeInMillis(),
                scheduleModel.getMessage(),
                scheduleModel.getScheduleStatus().getValue(),
                toRealmContacts(scheduleModel.getContactModels())
        );
    }
}
