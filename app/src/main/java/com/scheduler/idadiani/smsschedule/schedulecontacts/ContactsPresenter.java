package com.scheduler.idadiani.smsschedule.schedulecontacts;

import com.scheduler.idadiani.smsschedule.classes.ContactManager;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;
import com.scheduler.idadiani.smsschedule.mvp.presentation.Presenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by idadiani on 10/10/16.
 */

public class ContactsPresenter implements Presenter<ContactsView> {
    private ContactsView mView;
    private List<ContactModel> mContactModels;
    private List<ContactModel> mSelectedItems = new ArrayList<>();

    public ContactsPresenter() {
        mContactModels = new ArrayList<>();
    }

    @Override
    public void setView(ContactsView view) {
        mView = view;
    }

    @Override
    public void resume() {
        search("");
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    public void search(String s) {
        mContactModels = ContactManager.getContactsFast();
        if (s.length() > 0) {
            mContactModels = searchByQuery(mContactModels, s.toString());
        } else {
            mContactModels = ContactManager.getContactsFast();
        }
        sort(mContactModels);
        mView.populateList(mContactModels);
    }

    private List<ContactModel> searchByQuery(List<ContactModel> allContactModels, String searchQuery) {
        ArrayList<ContactModel> searchedContactModels = new ArrayList<>();
        for (ContactModel contactModel : allContactModels) {
            if (contactModel.getName().toLowerCase().contains(searchQuery.toLowerCase())) {
                searchedContactModels.add(contactModel);
            }
            if (contactModel.getPhoneNumber().contains(searchQuery)) {
                searchedContactModels.add(contactModel);
            }
        }
        return searchedContactModels;
    }

    private void sort(List<ContactModel> list) {
        Collections.sort(list, new Comparator<ContactModel>() {
            @Override
            public int compare(final ContactModel object1, final ContactModel object2) {
                return object1.getName().compareTo(object2.getName());
            }
        });
    }

    public void selectContacts(List<ContactModel> contactModels) {
        deselectAll();
        for (ContactModel contactModel : contactModels) {
            int id = getPositionInList(contactModel, mContactModels);
            mView.setItemChecked(id, true);
        }
    }

    private void deselectAll() {
        for (ContactModel contactModel : mContactModels) {
            int id = getPositionInList(contactModel, mContactModels);
            mView.setItemChecked(id, false);
        }
    }

    private int getPositionInList(ContactModel contactModel, List<ContactModel> contactModels) {
        for (int i = 0; i < contactModels.size(); i++) {
            ContactModel c = contactModels.get(i);
            if (contactModel.getPhoneNumber().equals(c.getPhoneNumber())) {
                return i;
            }
        }
        return -1;
    }

    public void loadContacts(ArrayList<ContactModel> contactModels) {
        mSelectedItems = contactModels;
        selectContacts(contactModels);
    }

    public void itemClicked(int position) {
        ContactModel contactModel = mContactModels.get(position);
        if (mSelectedItems.contains(contactModel)) {
            mSelectedItems.remove(contactModel);
            selectContacts(mSelectedItems);
        } else {
            mSelectedItems.add(contactModel);
            selectContacts(mSelectedItems);

        }

    }

    public void done() {
        mView.returnSelectedContacts(mSelectedItems);
    }
}
