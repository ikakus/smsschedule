package com.scheduler.idadiani.smsschedule.services;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.scheduler.idadiani.smsschedule.R;
import com.scheduler.idadiani.smsschedule.classes.SmsSender;
import com.scheduler.idadiani.smsschedule.enums.ContactStatus;
import com.scheduler.idadiani.smsschedule.enums.ScheduleStatus;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ContactModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.model.ScheduleModel;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryImpl;
import com.scheduler.idadiani.smsschedule.mvp.domain.repository.RepositoryInterface;
import com.scheduler.idadiani.smsschedule.schedulelist.ActivityScheduleList;

import java.util.List;

public class SmsSenderService extends Service {

    private static final String SENT = "SMS_SENT";
    private static final String DELIVERED = "SMS_DELIVERED";
    private Context mContext;
    private NotificationManager notificationManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mContext = this;
        RepositoryInterface repository = new RepositoryImpl();

        notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        SmsSender smsSender = new SmsSender(mContext);
        Long id = intent.getLongExtra("schedule_id", -1);
        ScheduleModel schedule = repository.getScheduleById(id);

        List<ContactModel> contactModels = schedule.getContactModels();
        if (contactModels != null && contactModels.size() > 0) {
            for (ContactModel contactModel : contactModels) {

                smsSender.sendSMS(contactModel.getPhoneNumber(), schedule.getMessage());
                smsReceiverRegister(contactModel);
            }
            schedule.setScheduleStatus(ScheduleStatus.SENT);
            repository.updateSchedule(schedule);
        } else {
            showNotification("No Contacts", (int) schedule.getId());
        }

        return Service.START_NOT_STICKY;
    }

    private void showNotification(String notificationText, int notificationId) {
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        Intent intent = new Intent(getApplicationContext(), ActivityScheduleList.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_my_main)
                .setTicker("SmsScheduler: " + notificationText)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle("Scheduler sent messages")
                .setContentText(notificationText);

        Notification notification = builder.build();
        notificationManager.notify(notificationId, notification);
    }

    private void smsReceiverRegister(final ContactModel contactModel) {
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(mContext, "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(mContext, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(mContext, "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(mContext, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(mContext, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //        ---when the SMS has been delivered---
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        showNotification("SMS delivered to: " + contactModel.getName()+ " " + contactModel.getPhoneNumber(), (int)contactModel.getId());
                        contactModel.setContactStatus(ContactStatus.SENT);
                        break;
                    case Activity.RESULT_CANCELED:
                        showNotification("SMS not delivered " + contactModel.getName() + contactModel.getPhoneNumber(),(int) contactModel.getId());
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

}
